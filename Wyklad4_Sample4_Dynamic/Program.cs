﻿using System;
using System.Dynamic;

namespace Wyklad4_Sample4
{

    class Kaczka
    {

    }

    class SmartKaczka : DynamicObject
    {
        public override bool TryInvokeMember(InvokeMemberBinder binder, object[] args, out object result)
        {
            Console.WriteLine("Urochomilaby sie metoda: " + binder.Name);
            foreach (var o in args)
                Console.WriteLine("Argument: " + o);
            result = null;
            return true;
        }
    }

    class Program
    {
        static void Main()
        {
            dynamic k = new Kaczka();
            // kompiluje się nawet, jeżeli taka metoda nie istnieje
            try
            {
                k.Kwak();
            }
            catch (Exception e)
            {
                Console.WriteLine($"Złapano wyjątek! {e.Message}");
            }
            dynamic k2 = new SmartKaczka();
            // kompiluje się i uruchomi nawet, jeżeli taka metoda jawnie nie istnieje
            k2.KwakKwak();
            Console.WriteLine();

            k2.Kwak("KuKu");
            k2.Kwak(123);

            Console.WriteLine("-------------------");
            Console.WriteLine("ExpandoObject sample");
            dynamic osoba = new ExpandoObject();
            osoba.Imie = "Ala";
            osoba.Nazwisko = "Kowalska";
            osoba.Wiek = 23;
            osoba.Wypisz = (Action)(() => { Console.WriteLine("{0} {1}",osoba.Imie, osoba.Nazwisko); });

            osoba.Wypisz();

            Console.ReadKey();

        }
    }
}
