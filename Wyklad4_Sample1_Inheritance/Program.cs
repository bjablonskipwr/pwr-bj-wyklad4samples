﻿using System;

namespace Wyklad4Sample1
{

    class BaseClass
    {
        public virtual void Foo() { Console.WriteLine("BaseClass.Foo"); }
    }

    class BaseClassNonVirtual
    {
        public void Foo() { Console.WriteLine("BaseClass.Foo"); }
    }

    class Overrider : BaseClass
    {
        public override void Foo() { Console.WriteLine("Overrider.Foo"); }
    }

    class Hider : BaseClass
    {
        public new void Foo() { Console.WriteLine("Hider.Foo"); }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Overrider o1 = new Overrider();
            BaseClass b1 = o1;
            object o = o1;
            
            Console.Write("o1: ");
            o1.Foo();
            Console.Write("b1: "); 
            b1.Foo();

            Console.Write("o: ");
            (o as BaseClass).Foo();


            Console.WriteLine("------------------");

            Hider h1 = new Hider();
            BaseClass b2 = h1;

            Console.Write("h1: ");
            h1.Foo();
            Console.Write("b2: ");
            b2.Foo();

            
            Console.ReadKey();

        }
    }
}
