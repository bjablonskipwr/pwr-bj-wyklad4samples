﻿using System;

namespace Wyklad4_Sample3
{
    class LambdaExpressionSample
    {
        public static int Przeksztalc(int x)
        {
            return x * x;
        }
        public static void Demo()
        {
            Console.WriteLine("---------------------");
            Console.WriteLine("Lambda expression sample");
            var a1 = new int[] { 1, 2, 3, 4, 5 };

            Transformer t2 = Przeksztalc;
            // Transformer t3 = x => x * x;
            MyUtil.Transform(a1, t2);
            foreach (var a in a1)
            {
                Console.WriteLine(a);
            }

            Console.WriteLine();
            MyUtil.Transform(a1, x => x + 1);
            foreach (var a in a1)
            {
                Console.WriteLine(a);
            }
        }

        public static void Demo2()
        {
            Console.WriteLine("---------------------");
            Console.WriteLine("Lambda expression sample - sumujDlugosc");

            Func<string, string, int> sumujDlugosc = (k1, k2) => { return k1.Length + k2.Length; };

            Action<string> wypiszDlugosc = s => { Console.WriteLine(s.Length); };

            Console.WriteLine(sumujDlugosc("Ala", "ma kota"));
            wypiszDlugosc("Ala ma kota");
        }
    }
}
