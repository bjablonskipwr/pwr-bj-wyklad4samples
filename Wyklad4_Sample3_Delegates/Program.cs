﻿using System;

namespace Wyklad4_Sample3
{

    class Program
    {

        static void Main(string[] args)
        {

            SimpleDelegateSample.Demo();

            ActionMulticastSample.Demo();

            EventsSample.Demo();

            LambdaExpressionSample.Demo();

            LambdaExpressionSample.Demo2();

            Console.ReadKey();

        }
    }
}
