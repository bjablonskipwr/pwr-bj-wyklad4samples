﻿using System;

namespace Wyklad4_Sample3
{
    public class MyStock
    {
        private decimal _price;
        
        public event EventHandler PriceChanged;
        
        protected virtual void OnPriceChanged(EventArgs e)
        {
            if (PriceChanged != null) PriceChanged(this, e);
        }

        public decimal Price
        {
            get { return _price; }
            set
            {
                if (_price == value) return;
                _price = value;
                OnPriceChanged(EventArgs.Empty);
            }
        }
    }
}
