﻿using System;

namespace Wyklad4_Sample3
{
    class ActionMulticastSample
    {
        static void Hello(string s)
        {
            Console.WriteLine("Hello, {0}", s);
        }

        static void Goodbye(string s)
        {
            Console.WriteLine("Goodbye, {0}", s);
        }

        public static void Demo()
        {
            Console.WriteLine("---------------------");
            Console.WriteLine("Action / multicast sample");
            // Action - standardowy delegat generyczny, który ma argumenty 
            // wejściowe, ale nic nie zwraca. 
            Action<string> actionDelegate = Hello;
            // Przykład multi-cast
            actionDelegate += Goodbye;

            // Uruchomione zostaną wszystkie metody przypisane do delegata a
            actionDelegate("Ala");
        }
    }
}
