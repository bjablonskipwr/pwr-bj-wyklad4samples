﻿using System;

namespace Wyklad4_Sample3
{
    public class SimpleDelegateSample
    {
        private static int Square(int x)
        {
            return x * x;
        }

        public static void Demo()
        {
            Console.WriteLine("---------------------");
            Console.WriteLine("Delegates sample");
            // Wykorzystanie delegatu jako mechanizmu plug-in
            var a1 = new int[] { 1, 2, 3, 4, 5 };
            MyUtil.Transform(a1, Square);

            foreach(var a in a1)
            {
                Console.WriteLine(a);
            }

        }
    }
}
