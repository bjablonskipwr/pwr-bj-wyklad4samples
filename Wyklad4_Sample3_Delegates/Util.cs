﻿namespace Wyklad4_Sample3
{
    public delegate int Transformer(int x);

    public class MyUtil
    {
        public static void Transform(int[] values, Transformer t)
        {
            for (var i = 0; i < values.Length; i++)
            {
                values[i] = t(values[i]);
            }
        }
    }
}
