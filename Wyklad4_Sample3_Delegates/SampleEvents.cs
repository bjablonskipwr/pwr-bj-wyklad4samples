﻿using System;

namespace Wyklad4_Sample3
{
    class EventsSample
    {
        public static void Demo()
        {
            Console.WriteLine("---------------------");
            Console.WriteLine("Events sample");
            // Przykład wykorzystania zdarzeń
            var ms = new MyStock();

            Console.WriteLine("Setting price to 10");
            ms.Price = 10;
            
            ms.PriceChanged += ms_PriceChanged;
            Console.WriteLine("Setting price to 20");
            ms.Price = 20;
        }

        private static void ms_PriceChanged(object sender, EventArgs e)
        {
            Console.WriteLine("Price changed! Sender: {0}, Sender Price:{1}", sender, (sender as MyStock)?.Price.ToString() ?? "N/A");
        }

    }
}
