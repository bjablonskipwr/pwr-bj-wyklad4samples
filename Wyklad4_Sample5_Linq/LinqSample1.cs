﻿using System.Collections.Generic;
using System.Linq;

namespace Wyklad4_Sample5
{
    public class LinqSample1
    {
        public static IEnumerable<string> Sample(IEnumerable<string> sekwencja)
        {
            var query = sekwencja
                .Where(n => n.Contains("e"))
                .OrderBy(n => n.Length)
                .Select(n => n.ToUpper())
                .Select(n =>
                {
                    var s = "";
                    foreach (char c in n)
                        s = s + c + " ";
                    return s;
                }
                );

            return query;
        }


        public static bool SecretMethod(string s)
        {
            return s
                .GroupBy((c) => c)
                .Select(group => group.Count() % 2 == 0 ? 0 : 1)
                .Sum()
                .Equals(s.Length % 2);
        }

    }
}
