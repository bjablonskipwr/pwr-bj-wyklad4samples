﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Wyklad4_Sample5
{
    public class Customer
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class Order
    {
        public int CustomerId { get; set; }
        public int ProductId { get; set; }
    }

    public class Sklep
    {
        readonly IEnumerable<Customer> _customers = new Customer[] {
                new Customer { Id = 1, Name = "Janek"},
                new Customer { Id = 2, Name = "Kasia"},
                new Customer { Id = 3, Name = "Jola"},
                };

        readonly IEnumerable<Product> _products = new Product[]{
                new Product { Id = 1, Name="Komputer"},
                new Product { Id = 2, Name="Telewizor"},
                new Product { Id = 3, Name="Lampa"},
                };

        readonly IEnumerable<Order> _orders = new Order[] {
                new Order{ CustomerId = 1, ProductId = 1},
                new Order{ CustomerId = 1, ProductId = 2},
                new Order{ CustomerId = 2, ProductId = 3},
                };

        public IEnumerable<object> Report1()
        {
            var raport = from c in _customers
                         from p in _products
                         from o in _orders
                         where o.CustomerId == c.Id && o.ProductId == p.Id
                         select c.Name + " kupił " + p.Name;
            return raport;
        }

        public IEnumerable<object> Report2()
        {
            var raport2 = from c in _customers
                          join o in _orders on c.Id equals o.CustomerId
                          join p in _products on o.ProductId equals p.Id
                          select new { Klient = c.Name, Produkt = p.Name };

            return raport2;
        }

        public void Report3()
        {
            var raport3 = from c in _customers
                          group c.Name by c.Name.First();
            foreach (var g in raport3)
            {
                Console.WriteLine("Key = " + g.Key);
                foreach (var v in g)
                    Console.WriteLine(v);
            }
        }

        public void Report4()
        {
            var raport4 = from c in _customers
                          join o in _orders on c.Id equals o.CustomerId
                          join p in _products on o.ProductId equals p.Id
                          select new { Klient = c.Name, Produkty = p.Name } into zamowienia
                          group zamowienia.Produkty by zamowienia.Klient;

            foreach (var klient in raport4)
            {
                Console.WriteLine("Klient name=" + klient.Key);
                foreach (var produkt in klient)
                {
                    Console.WriteLine("Produkt " + produkt);
                }
                Console.WriteLine("------------");
            }

        }


        public static void Display(IEnumerable<object> sequence)
        {
            foreach (var obj in sequence)
            {
                Console.WriteLine(obj);
            }
        }

    }
}
