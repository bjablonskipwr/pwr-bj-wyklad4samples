﻿using System;

namespace Wyklad4_Sample5
{
    class Program
    {
        private static void Main()
        {
            Console.WriteLine("-------------------- Linq Sample: Imiona");
            string[] imiona = { "Jacek", "Ola", "Agata", "Ania", "Karol", "Stefan", "Mateusz" };

            var res = LinqSample1.Sample(imiona);

            foreach (var s in res)
            {
                Console.WriteLine(s);
            }

            Console.WriteLine("-------------------- Linq Sample: Sklep");
            var sklep = new Sklep();
            Sklep.Display(sklep.Report1());
            Sklep.Display(sklep.Report2());
            sklep.Report3();
            sklep.Report4();

            #region Secret method
            Console.WriteLine("-------------------- Linq Sample: Secret Method");
            Console.WriteLine(LinqSample1.SecretMethod("alaMaKota"));
            #endregion

            Console.ReadKey();

        }

    }
}
