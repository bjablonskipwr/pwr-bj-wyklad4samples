﻿using System;

namespace Wyklad4_Sample2
{
    public class MyStack<T>
    {
        private int _position;
        private readonly T[] _data = new T[100];

        public void Push(T obj) { _data[_position++] = obj; }
        public T Pop() { return _data[--_position]; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            MyStack<int> stack = new MyStack<int>();
            stack.Push(10);
            stack.Push(11);
            Console.WriteLine(stack.Pop());
            Console.WriteLine(stack.Pop());

            Console.WriteLine("-----------");

            MyStack<double> stackD = new MyStack<double>();
            stackD.Push(10.1);
            stackD.Push(12.1);
            Console.WriteLine(stackD.Pop());
            Console.WriteLine(stackD.Pop());

            Console.ReadKey();
        }
    }
}
